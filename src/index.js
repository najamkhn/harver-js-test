const { generateWords, fizzBuzzSync, fizzBuzz } = require('./lib');
const fs = require('fs');

const FIZZ_ON = 3;
const BUZZ_ON = 5;
const LIMIT = 100;
const arrayList = [...Array(LIMIT).keys()].map(x => x + 1);
const noop = () => {};

/**
 * 1. Print numbers from 1 to 100 to the console, but for each number also print
 * a random word using the function `getRandomWordSync`. E.g.
 * 
 * @param {Array} list 
 */
const step1 = list => {
    return list.map((item, index) => `${index}: ${generateWords(index)}`);
}

/**
 * 2.  Modify your code to be a "Fizz Buzz" program. That is, print the numbers
 * as in the previous step, but for multiples of three, print "Fizz" (instead of
 * the random word), for multiples of five, print "Buzz" and for numbers which
 * are both multiples of three and five, print "FizzBuzz".
 *  
 * @param {Array} list 
 */
const step2 = list => {
    return list.map((item, index) => `${index}: ${fizzBuzzSync(item, FIZZ_ON, BUZZ_ON)}`);
}

/**
 * 3. Create a version of steps *1* and *2* using the **asynchronous** function,
 * `getRandomWord`. This function returns a Promise, which resolves to a random
 * word string. The numbers may or may not be in numerical order.
 * 
 * @param {Array} list 
 */
const step3 = list => {
    return Promise.all(
        list.map((item, index) => fizzBuzz(item, FIZZ_ON, BUZZ_ON))
    );
}


/** 
 * 4. Add error handling to both the synchronous and asynchronous solutions
 * (calling `getRandomWord({ withErrors: true })` will intermitently throw an
 * error instead of return a random word). When an error is caught, the programm
 * should print "Doh!" instead of the random word, "Fizz", "Buzz" or "FizzBuzz"
 * 
 * @param {Array} list 
 */
const step4 = list => {
    return Promise.all(
        list.map((item, index) => fizzBuzz(item, FIZZ_ON, BUZZ_ON, true).catch(x => 'Doh'))
    );
}

/**
 * 5. For **Node.JS developers**: Instead of printing the console. Write the
 * information to a file in the root of this project. For **Frontend**
 * developers, send your result to an HTTP endpoint (since there is no running
 * endpoint, this part of your solution does not need to actually run)
 * 
 * @param {*} list 
 */

const step5 = (filename, text, cb) => {
    return fs.writeFile(filename, text, cb);
}

// Aliasing it so it doesn't look confusing later in the code
const writeFile = step5;

(list => {

    // Writing output of each step to their respective file
    writeFile('./step1', step1(list).join('\n'), noop);
    writeFile('./step2', step2(list).join('\n'), noop);

    step3(list)
        .then(values => values.map((word, index) => `${index + 1}: ${word}`).join('\n'))
        .then(text => writeFile('./step3', text, noop));

    step4(list)
        .then(values => values.map((word, index) => `${index + 1}: ${word}`).join('\n'))
        .then(text => writeFile('./step4', text, noop));
        
}) (arrayList);