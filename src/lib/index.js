const { getRandomWordSync, getRandomWord } = require('word-maker');

const generateWords = () => (getRandomWordSync());

const fizzBuzzSync = (index, fizzOn, buzzOn, withErrors = false) => {
    let randomWord = '';
    
    if (index % fizzOn === 0) {
        randomWord = 'Fizz';
    }
    
    if (index % buzzOn === 0) {
        randomWord += 'Buzz'
    } 
    
    if (randomWord.length <= 0) {
        randomWord = getRandomWordSync({ withErrors });
    }

    return randomWord;
}


const fizzBuzz = (index, fizzOn, buzzOn, withErrors = false) => {
    let randomWord = '';
    
    if (index % fizzOn === 0) {
        randomWord = 'Fizz';
    }
    
    if (index % buzzOn === 0) {
        randomWord += 'Buzz'
    } 
    
    if (randomWord.length <= 0) {
        return getRandomWord({ withErrors: withErrors });
    }

    return Promise.resolve(randomWord);
}


module.exports = { generateWords, fizzBuzzSync, fizzBuzz };
